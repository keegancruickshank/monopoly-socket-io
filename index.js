var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var path = require('path');
var bodyParser = require("body-parser");
var cors = require('cors');
var ws281x = require('rpi-ws281x');


var WHITECOLOR = "0xffffff";
var GREENCOLOR = "0xff0000";
var BLUECOLOR = "0x0000ff";
var PURPLECOLOR = "0x00ffff";
var YELLOWCOLOR = "0xffff00";
var ORANGECOLOR = "0xa5ff00";
var REDCOLOR = "0x00ff00";

app.use(cors());
app.use(bodyParser.json());

var configured = false;

app.post('/startBoard', function(req, res){
  if(configured == false) {
    ws281x.configure({leds:28, brightness: req.body.brightness});
    configured = true;
  }
  console.log("Start Game");
  var pixels = new Uint32Array(28);
  var even = false;
  var timer = setInterval(function() {
    for(let i = 0; i<28; i++) {
      if(i%2) {
        if(even) {
          pixels[i] = "0x00ff00";
        } else {
          pixels[i] = "0xffffff";
        }
      } else {
        if(even) {
          pixels[i] = "0xffffff";
        } else {
          pixels[i] = "0x00ff00";
        }
      }
    }
    ws281x.render(pixels);
    even = !even
  }, 1000);
  setTimeout(function() {
    clearInterval(timer);
    for(let i = 0; i<28; i++) {
      pixels[i] = "0x777777";
    }
    ws281x.render(pixels);
  }, 5000)
  res.send("Completed");
});

app.post('/setBoardColor', function(req, res){
  if(configured == false) {
    ws281x.configure({leds:28, brightness: req.body.brightness});
    configured = true;
  }
  console.log("Set Color Requested");
  var pixels = new Uint32Array(28);
  for(let i = 0; i<28; i++) {
    console.log("Pixel " + i + " - Set Color " + req.body.data[i]);
    pixels[i] = req.body.data[i];
  }
  res.send("Completed");
  ws281x.render(pixels);
});

io.on('connection', function(socket){
  console.log('User Connected');
});

http.listen(3000, function(){
  console.log('Running on *:3000');
});

var spawn = require("child_process").spawn;
var process = spawn('python', [
    "-u",
    path.join(__dirname, 'scripts/monopoly.py'),
 ]);
process.stdout.on('data', function (data) {
  console.log(data);
  io.emit("scan", data.toString());
});
